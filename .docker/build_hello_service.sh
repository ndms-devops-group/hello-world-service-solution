# Application: Hello World Service
# Description: Build the Hello World Service using Docker Compose
# Author:  dempsey
# Since:  0.0.1

docker-compose -f docker-compose.yml -f docker-compose.build.yml build