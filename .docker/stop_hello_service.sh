#!/usr/bin/env bash
# Application: Hello World Service
# Description: Stop the Hello World Service
# Author:  dempsey
# Since:  0.0.1

docker-compose -f docker-compose.yml down