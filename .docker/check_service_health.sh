#!/usr/bin/env bash
# Application: Hello World Service
# Description: Check the health status of the Hello World Service.
# exit codes:
#  0:  service is healthy
#  1:  service failed the health check
# Author:  dempsey
# Since:  0.0.1

CONTAINER_NAME=$(docker ps --format '{{.Names}}' | grep hello-service)

echo "Hello Service Container name:  $CONTAINER_NAME"

if [ "${CONTAINER_NAME}" = "" ];
then
  echo "error:  unable to find the Hello Service Container"
  exit 1
fi

HEALTH_STATUS=$(docker inspect --format '{{json .State.Health.Status}}' ${CONTAINER_NAME})
echo "Container health status:  $HEALTH_STATUS"
if [ "$HEALTH_STATUS" = "\"healthy\"" ] || [ "$HEALTH_STATUS" = "\"starting\"" ]
then
  exit 0
else
  exit 1
fi
